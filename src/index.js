import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';

import { TaskProvider } from './context/task-context';

import './index.css';

import App from './App';

render((
  <TaskProvider>
    <Router>
      <App/>
    </Router>
  </TaskProvider>
), document.getElementById('root'));

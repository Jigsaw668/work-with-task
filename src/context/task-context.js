import React, { createContext, Component } from 'react';

import { GUID } from '../configuration/tenantguid';

const TaskContext = createContext({
  task: [],
  priorities: []
});

export class TaskProvider extends Component {
  state = {
    task: [],
    priorities: [],
    taskUpdate: () => {
      fetch(`http://intravision-task.test01.intravision.ru/odata/tasks?tenantguid=${GUID}`)
        .then(res => res.json())
        .then(res => this.setState({ task: res.value }))
        .catch(() => this.setState({ task: [] }));
    }
  };

  componentDidMount() {
    fetch(`http://intravision-task.test01.intravision.ru/odata/tasks?tenantguid=${GUID}`)
      .then(res => res.json())
      .then(res => this.setState({ task: res.value }))
      .catch(() => this.setState({ task: [] }));

    fetch(`http://intravision-task.test01.intravision.ru/api/${GUID}/Priorities`)
      .then(res => res.json())
      .then(res => this.setState({ priorities: res }))
      .catch(() => this.setState({ priorities: [] }));
  }

  render() {
    return (
      <TaskContext.Provider value={this.state}>
        {this.props.children}
      </TaskContext.Provider>
    );
  }
}

export const TaskConsumer = TaskContext.Consumer;

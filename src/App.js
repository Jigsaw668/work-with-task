import React from 'react';
import { Route } from 'react-router-dom';

import Layout from './components/layout';

import WhiteList from './components/whiteList';
import TaskTable from './components/taskTable';

import './App.css';

const App = () => {
  return (
    <Layout>
      <Route exact path='/base' component={ WhiteList } />
      <Route exact path='/applications' component={ TaskTable } />
      <Route exact path='/employees' component={ WhiteList } />
      <Route exact path='/customers' component={ WhiteList } />
      <Route exact path='/assets' component={ WhiteList } />
      <Route exact path='/settings' component={ WhiteList } />
    </Layout>
  );
}

export default App;

import React, { createContext } from 'react';

import { GUID } from './configuration/tenantguid';

const UserContext = createContext({
  prioritet: [],
  tags: [],
  status: [],
  openCreate: false,
  updateOpenCreate: () => {},
  task: {},
});

export class UserProvider extends React.Component {
  updateOpenCreate = isOpen => {
    this.setState({ openCreate: isOpen });
  };

  state = {
    prioritet: [],
    tags: [],
    status: [],
    openCreate: false,
    updateOpenCreate: this.updateOpenCreate,
    task: {},
  };

  componentDidMount() {
    fetch(`http://intravision-task.test01.intravision.ru/api/${GUID}/Priorities`)
      .then(res => res.json())
      .then(res => this.setState({ prioritet: res }))
      .catch(() => this.setState({ prioritet: [] }));

    fetch(`http://intravision-task.test01.intravision.ru/api/${GUID}/Tags`)
      .then(res => res.json())
      .then(res => this.setState({ tags: res }))
      .catch(() => this.setState({ tags: [] }));

    fetch(`http://intravision-task.test01.intravision.ru/api/${GUID}/Statuses`)
      .then(res => res.json())
      .then(res => this.setState({ status: res }))
      .catch(() => this.setState({ status: [] }));

    fetch(`http://intravision-task.test01.intravision.ru/odata/tasks?tenantguid=${GUID}`)
      .then(res => res.json())
      .then(res => this.setState({ task: res.value }))
      .catch(() => this.setState({ task: [] }));
  }

  render() {
    return (
      <UserContext.Provider value={this.state}>
        {this.props.children}
      </UserContext.Provider>
    );
  }
}

export const UserConsumer = UserContext.Consumer;

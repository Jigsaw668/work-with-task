import React from 'react';
import { NavLink } from 'react-router-dom';

import book from '../../img/book.png';
import city from '../../img/city.png';
import list from '../../img/list.png';
import logo from '../../img/logo.png';
import monitor from '../../img/monitor.png';
import people from '../../img/people.png';
import settings from '../../img/settings.png';

import './Navigation.scss';

const Navigation = () => {
  return (
    <div className='nav-wrapp'>
      <img src={logo} alt='logo' className='logo' />
      <nav className='nav'>
        <NavLink to='/base' className='nav__item' activeClassName='current-page'>
          <img src={book} alt='base' className='nav__icon' />
          База знаний
        </NavLink>
        <NavLink to='/applications' className='nav__item' activeClassName='current-page'>
          <img src={list} alt='applications' className='nav__icon' />
          Заявка
        </NavLink>
        <NavLink to='/employees' className='nav__item' activeClassName='current-page'>
          <img src={people} alt='employees' className='nav__icon' />
          Сотрудники
        </NavLink>
        <NavLink to='/customers' className='nav__item' activeClassName='current-page'>
          <img src={city} alt='customers' className='nav__icon' />
          Клиенты
        </NavLink>
        <NavLink to='/assets' className='nav__item' activeClassName='current-page'>
          <img src={monitor} alt='assets' className='nav__icon' />
          Активы
        </NavLink>
        <NavLink to='/settings' className='nav__item' activeClassName='current-page'>
          <img src={settings} alt='settings' className='nav__icon' />
          Настройки
        </NavLink>
      </nav>
    </div>
  );
};

export default Navigation;

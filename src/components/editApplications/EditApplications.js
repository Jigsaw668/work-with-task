import React from 'react';

import { GUID } from '../../configuration/tenantguid';

import './EditApplications.scss';

const monthArr = [
  'Январь',
  'Февраль',
  'Март',
  'Апрель',
  'Май',
  'Июнь',
  'Июль',
  'Август',
  'Сентябрь',
  'Ноябрь',
  'Декабрь',
]

const stat = {
  20982: '#3cb371',
  20983: '#909090',
  20984: '#fcad51',
  20985: '#fcad51',
  20986: '#fd5e53',
  20987: '#025969'
}

export default class EditApplications extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      task: {},
      comment: '',
      executorId: 0,
      statusId: 0,
      statuses: [],
      users: [],
      currentData: {
        executorId: 0,
        statusId: 0
      }
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    fetch(`http://intravision-task.test01.intravision.ru/api/${GUID}/Tasks/${this.props.idEdit}`)
      .then(res => res.json())
      .then(res => this.setState({
        task: res,
        executorId: res.executorId,
        statusId: res.statusId,
        currentData: {
          executorId: res.executorId,
          statusId: res.statusId
        }
      }))
    fetch(`http://intravision-task.test01.intravision.ru/api/${GUID}/Statuses`)
      .then(res => res.json())
      .then(res => this.setState({ statuses: res }))
    fetch(`http://intravision-task.test01.intravision.ru/api/${GUID}/Users`)
      .then(res => res.json())
      .then(res => this.setState({ users: res }))
  }

  handleChange(event) {
    this.setState({[event.target.name]: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();

    const emptyComment = this.state.comment === '';
    const changeStatusId = this.state.currentData.statusId !== this.state.statusId;
    const changeExecutorId = this.state.currentData.executorId !== this.state.executorId;

    console.log(!emptyComment, changeStatusId, changeExecutorId);

    if (!emptyComment || changeStatusId || changeExecutorId) {
      console.log('1');
      let data = {}

      if (!emptyComment) {
        data = {
          id: +this.props.idEdit,
          statusId: +this.state.statusId,
          executorId: +this.state.executorId,
          comment: this.state.comment
        }
      } else {
        data = {
          id: +this.props.idEdit,
          statusId: +this.state.statusId,
          executorId: +this.state.executorId,
        }
      }

      fetch(`http://intravision-task.test01.intravision.ru/api/${GUID}/Tasks`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
      })

      .then(() => {
        fetch(`http://intravision-task.test01.intravision.ru/api/${GUID}/Tasks/${this.props.idEdit}`)
        .then(res => res.json())
        .then(res => this.setState({ task: res }))
      })
      .then(() => this.props.taskUpdate())
    }
  }

  getDate = (date) => {
    const day = date.slice(8, 10);
    const month = monthArr[date.slice(5, 7)]
    const time = date.slice(11, 16);

    return day + ' ' + month + ', ' + time;
  }

  render() {
    return (
      <div className='edit-field'>
        <div className='edit__header'>
          <div className='edit__header-title'>
            <span className='edit__title'>№</span>
            <div className='edit__title edit__title-number'>{this.props.idEdit}</div>
            <h3 className='edit__title'>{this.state.task.name}</h3>
          </div>
          <button className='edit__close' name='isOpenEdit' onClick={() => this.props.openEdit(false)}></button>
        </div>
        <div className='edit__body'>
          <div className='edit__body-left'>
            <div className='edit__description'>
              <span className='edit__label'>Описание</span>
              <p className='edit__text'>{this.state.task.description}</p>
            </div>
            <form className='edit__body-form' onSubmit={this.handleSubmit}>
              <label className='edit-form__label'>
                Добавление коментариев
                <textarea className='edit-form__textarea' name='comment' value={this.state.comment} onChange={this.handleChange}></textarea>
              </label>
              <input className='edit-form__submit' value='Сохранить' type='submit' />
            </form>
            { this.state.task.lifetimeItems &&
              this.state.task.lifetimeItems.map((item, key) => {
                if (item.comment !== null) {
                  return (
                    <div key={key} className='edit-comment'>
                      <div className='edit-comment__title'>
                        <div className='edit-comment__icon'></div>
                        <div className='edit-comment__time'>
                          <p className='edit-comment__name'>{this.state.task.initiatorName}</p>
                          <p className='edit-comment__date'>{this.getDate(this.state.task.createdAt)} прокоментировал</p>
                        </div>
                      </div>
                      <div className='edit-comment__text'>
                        <p className='edit-comment__p'>{item.comment}</p>
                      </div>
                    </div>
                  )
                }
              })
            }
          </div>
          <div className='edit__body-right'>
            <div className='edit-status'>
              <div className='edit-status__circle' style={{backgroundColor: stat[this.state.statusId]}}></div>
              <label className='edit-name__label'>
                { this.state.statuses.length !== 0 &&
                  <select className='edit-status__select' name='statusId' value={this.state.statusId} onChange={this.handleChange}>
                    {
                      this.state.statuses.map((status, key) => {
                        return (
                          <option key={key} value={status.id}>{status.name}</option>
                        )
                      })
                    }
                  </select>
                }
              </label>
            </div>
            <div className='edit__field'>
              <div className='edit-name__label'>Заявитель</div>
              <div>{this.state.task.initiatorName}</div>
            </div>
            <div className='edit__field'>
              <div className='edit-name__label'>Создана</div>
              <div>{this.state.task.initiatorName}</div>
            </div>
            <div className='edit__field'>
              <label className='edit-name__label'>
                Исполнитель
                { this.state.users.length !== 0 &&
                  <select className='edit-status__select edit-status__select_large' name='executorId' value={this.state.executorId} onChange={this.handleChange}>
                    {
                      this.state.users.map((user, key) => {
                        return (
                          <option key={key} value={user.id}>{user.name}</option>
                        )
                      })
                    }
                  </select>
                }
              </label>
            </div>
            <div className='edit__field'>
              <div className='edit-name__label'>Приоритет</div>
              <div>{this.state.task.priorityName}</div>
            </div>
          </div>
        </div>
      </div>
    );
  };
};

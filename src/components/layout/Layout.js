import React, { Component } from 'react';

import Navigation from '../navigation';
import SearchBar from '../searchBar';

import './Layout.scss';

export default class Layout extends Component {
  render() {
    return (
      <div className='wrapper'>
        <Navigation />
        <SearchBar />
        <div className='main-wrapp'>
          <main className='content'>
            {this.props.children}
          </main>
        </div>
      </div>
    );
  }
}

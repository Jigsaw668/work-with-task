import React from 'react';

import './SearchBar.scss';

const SearchBar = () => {
  return (
    <div className='search-wrapp'>
      <input type='text' className='search-bar'>
      </input>
    </div>
  );
};

export default SearchBar;

import React from 'react';

import { GUID } from '../../configuration/tenantguid';

import './CreateApplication.scss';

export default class CreateApplication extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      description: ''
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    fetch(`http://intravision-task.test01.intravision.ru/api/${GUID}/Tasks`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "name": this.state.value,
          "description": this.state.description,
          "statusId": 20986
        })
      })
      .then(res => res.json())
      .then(res => this.props.updateID(res))
      .then(() => this.props.openEdit(true))
      .then(() => {
        this.setState({
          value: '',
          description: ''
        })
      })
      .then(() => this.props.isOpen(false))
      .then(() => {
        this.props.taskUpdate();
      })
  }

  render() {
    return (
      <div className='create-field'>
        <div className='create__header'>
          <h3 className='create__title'>Новая заявка</h3>
          <button className='create__close' name='isOpenCreate' onClick={() => this.props.isOpen(false)}></button>
        </div>
        <form onSubmit={this.handleSubmit} className='create__form'>
          <label className='form__textare-label'>
            Название
            <textarea className='form__textarea' name='value' value={this.state.value} onChange={this.handleChange.bind(this)} required></textarea>
          </label>
          <label className='form__textare-label'>
            Описание
            <textarea className='form__textarea' name='description' value={this.state.description} onChange={this.handleChange.bind(this)} required></textarea>
          </label>
          <input className='form__submit' type='submit' value='Сохранить' />
        </form>
      </div>
    )
  }
}
import React from 'react';

import './WhiteList.scss';

const WhiteList = () => {
  return (
    <div>
      <h1 className='page-name'>
        {
          pageName[window.location.pathname.substr(1)]
        }
      </h1>
    </div>
  );
};

const pageName = {
  base: 'База знаний',
  employees: 'Сотрудники',
  customers: 'Клиенты',
  assets: 'Активы',
  settings: 'Настройки',
}

export default WhiteList;
